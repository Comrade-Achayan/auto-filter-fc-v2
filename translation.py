#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# (c) @AlbertEinsteinTG

class Translation(object):
    
    START_TEXT = """<b>Hey {}!! My name is {}🙃🙃</b>

<b><i>നീ ഏതാ മോനൂസെ എന്നെ </i></b> <a href="https://t.me/joinchat/3mIJE352G7AzZmY1">𝑭𝒊𝒍𝒎 𝑪𝒐𝒎𝒑𝒂𝒏𝒚</a> <b><i>ഗ്രൂപ്പിൽ മാത്രമേ ഉപയോഗിക്കാൻ പറ്റൂ...

വെറുതെ സമയം കളയാൻ നിൽക്കണ്ട...വേഗം ഗ്രൂപ്പിലേക്ക് വിട്ടോ സിനിമ അവിടെ കിട്ടും...🤭🤭</i></b>"""
    
